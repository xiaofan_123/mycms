<?php

namespace Expand\Api\handles;

use Expand\Api\ApiHandleInterface;

class DemoApiHandle implements ApiHandleInterface
{

    public function handle($params): array
    {
        return ['content' => 'PHP是全宇宙最好的语言'];
    }

    public function getName(): string
    {
        return '自定义接口示例';
    }
}
